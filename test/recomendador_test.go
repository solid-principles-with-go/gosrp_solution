// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* --                    recomendador_test.go                        */
/* --                                                                */
/* --  Descripcion: Contiene las pruebas unitarias del ejercicios    */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/model"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/peliculas"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/recomendador"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/recomendadorCSV"
)

func TestGetRecomendaciones(t *testing.T) {
	c := model.GetClienteX()
	r := recomendador.GetRecomendaciones(c)
	if peliculas.ContainsPeliculas(r, model.RYAN.Titulo) {
		t.Errorf("No deberia existir <%s>", model.RYAN.Titulo)
	}
}

func TestFormatoCSV(t *testing.T) {
	c := model.GetClienteY()
	str := recomendadorCSV.GetRecomendacionesCSV(c)
	assert.Equal(t, str, "Tormenta Perfecta, Wolfgang Petersen, Acción\n", "The two outputs should be the same.")
}
