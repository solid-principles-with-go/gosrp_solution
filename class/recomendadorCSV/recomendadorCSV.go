// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* --                          recomendador.go                       */
/* --                                                                */
/* --  Descripcion: Contiene todo el codigo de la logica del         */
/* --               ejercicio.                                       */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package recomendadorCSV

import (
	"fmt"

	cc "gitlab.com/rodrigoghm/gosrp_ejercicio/class/clientes"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/recomendador"
)

func GetRecomendacionesCSV(c cc.Clientes) string {
	P := recomendador.GetRecomendaciones(c)
	var cad string

	for _, peli := range P {
		cad = cad + fmt.Sprintf("%s, %s, %s\n", peli.GetTitulo(), peli.GetDirector(), peli.GetGenero())
	}

	return cad
}
