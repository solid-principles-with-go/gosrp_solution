// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* --                          recomendador.go                       */
/* --                                                                */
/* --  Descripcion: Contiene todo el codigo de la logica del         */
/* --               ejercicio.                                       */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package recomendador

import (
	cc "gitlab.com/rodrigoghm/gosrp_ejercicio/class/clientes"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/model"
	pp "gitlab.com/rodrigoghm/gosrp_ejercicio/class/peliculas"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/utils"
)

func GetRecomendaciones(c cc.Clientes) []pp.Peliculas {
	var recomendadas, def []pp.Peliculas
	var directores []string
	fav := c.GetFavoritas()

	for _, m := range fav {
		if !utils.Contains(directores, m.GetDirector()) {
			directores = append(directores, m.GetDirector())
			recomendadas = append(recomendadas, model.PelisPorDirector(m.GetDirector())...)
		}
	}

	for _, m := range recomendadas {
		if !pp.ContainsPeliculas(fav, m.GetTitulo()) {
			def = append(def, m)
		}
	}

	return def
}
