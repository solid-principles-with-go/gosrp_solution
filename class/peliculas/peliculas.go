// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          peliculas.go                          */
/* --                                                                */
/* --  Descripcion: Contiene todo el modelo (BBDD) del ejercicio     */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package peliculas

func SetPeliculas(t string, g string, d string) Peliculas {
	var P Peliculas
	P.Titulo = t
	P.Genero = g
	P.Director = d
	return P
}

func (p Peliculas) GetTitulo() string {
	return p.Titulo
}

func (p Peliculas) GetGenero() string {
	return p.Genero
}

func (p Peliculas) GetDirector() string {
	return p.Director
}

func ContainsPeliculas(s []Peliculas, t string) bool {
	for _, v := range s {
		if v.Titulo == t {
			return true
		}
	}

	return false
}
