// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/*********************************************************************/
/*********************************************************************/
/*                         clientes.go                               */
/*                                                                   */
/* Descripcion: clase de clientes.                                   */
/*                                                                   */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>          */
/*                                                                   */
/* © 2022 - Mercado Libre - Desafio Tecnico SOLID                    */
/*********************************************************************/

package clientes

import "gitlab.com/rodrigoghm/gosrp_ejercicio/class/peliculas"

func SetClientes(n string, pf []peliculas.Peliculas) Clientes {
	var C Clientes
	C.Nombre = n
	C.Favoritas = pf
	return C
}

func (c Clientes) GetNombre() string {
	return c.Nombre
}

func (c Clientes) GetFavoritas() []peliculas.Peliculas {
	fv := make([]peliculas.Peliculas, 10)
	for _, cl := range c.Favoritas {
		fv = append(fv, cl)
	}

	return fv
}
