// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          model.go                              */
/* --                                                                */
/* --  Descripcion: Contiene todo el modelo (BBDD) del ejercicio     */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package model

const (
	DIRECTOR_SPIELBERG = "Steven Spielberg"
	DIRECTOR_CAMERON   = "James Cameron"
	DIRECTOR_MICHELL   = "Roger Michell"
	DIRECTOR_PETERSEN  = "Wolfgang Petersen"
	DIRECTOR_SCOTT     = "Ridley Scott"
	GENERO_FICCION     = "Ficción"
	GENERO_ROMANCE     = "Romance"
	GENERO_ACCION      = "Acción"
)
