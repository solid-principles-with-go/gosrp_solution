// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
// -- ****************************************************************/
/*                         model.go                                  */
/*                                                                   */
/* Descripcion: Contiene todo el modelo (BBDD) del ejercicio         */
/*                                                                   */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>          */
/*                                                                   */
/* © 2022 - Mercado Libre - Desafio Tecnico SOLID                    */
// -- ****************************************************************/

package model

import (
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/clientes"
	p "gitlab.com/rodrigoghm/gosrp_ejercicio/class/peliculas"
)

var ET = p.Peliculas{
	Titulo:   "ET",
	Genero:   GENERO_FICCION,
	Director: DIRECTOR_SPIELBERG}
var TITANIC = p.Peliculas{Titulo: "Titanic", Genero: GENERO_ROMANCE, Director: DIRECTOR_CAMERON}
var NHILL = p.Peliculas{Titulo: "Notting Hill", Genero: GENERO_ROMANCE, Director: DIRECTOR_MICHELL}
var TROYA = p.Peliculas{Titulo: "Troya", Genero: GENERO_ACCION, Director: DIRECTOR_PETERSEN}
var TORMENTA = p.Peliculas{Titulo: "Tormenta Perfecta", Genero: GENERO_ACCION, Director: DIRECTOR_PETERSEN}
var GLADIADOR = p.Peliculas{Titulo: "Gladiador", Genero: GENERO_ACCION, Director: DIRECTOR_SCOTT}
var TERMINATOR = p.Peliculas{Titulo: "Terminator", Genero: GENERO_ACCION, Director: DIRECTOR_CAMERON}
var AVATAR = p.Peliculas{Titulo: "Avatar", Genero: GENERO_FICCION, Director: DIRECTOR_CAMERON}
var RYAN = p.Peliculas{Titulo: "Rescatando al Soldado Ryan", Genero: GENERO_ACCION, Director: DIRECTOR_SPIELBERG}
var SCHINDLER = p.Peliculas{Titulo: "La lista de Schindler", Genero: GENERO_ACCION, Director: DIRECTOR_SPIELBERG}

func GetAllPeliculas() []p.Peliculas {

	movies := []p.Peliculas{}
	movies = append(movies, ET)
	movies = append(movies, TITANIC)
	movies = append(movies, NHILL)
	movies = append(movies, TROYA)
	movies = append(movies, TORMENTA)
	movies = append(movies, GLADIADOR)
	movies = append(movies, TERMINATOR)
	movies = append(movies, AVATAR)
	movies = append(movies, RYAN)
	movies = append(movies, SCHINDLER)

	// -- retornamos nuestra BBDD de peliculas.
	return movies
}

func GetClienteX() clientes.Clientes {
	x := clientes.SetClientes("X", []p.Peliculas{ET, RYAN, GLADIADOR})
	return x
}

func GetClienteY() clientes.Clientes {
	y := clientes.SetClientes("Y", []p.Peliculas{TROYA})
	return y
}

func PelisPorDirector(dir string) []p.Peliculas {
	movies := []p.Peliculas{}
	todas := GetAllPeliculas()

	for _, p := range todas {
		if p.Director == dir {
			movies = append(movies, p)
		}
	}
	return movies
}
