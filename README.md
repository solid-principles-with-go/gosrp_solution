# Single Responsability Principle

Este Caso de Uso representa una aplicacion de recomendacion de peliculas.

El codigo que nos interesa esta en el package "recomendador" en el script "recomendador.go", Esta 'clase' tiene dos metodos:
- recomendaciones: devuelve la lista de recomendaciones para un cliente
- recomendacionesCSV : devuelve la lista de recomendaciones en formato CSV (comma separated value)

Una pelicula tiene titulo, director y genero
Un cliente tiene nombre y lista de peliculas favoritas

El algoritmo de recomendacion se basa en proponer peliculas de directores que han gustado al cliente.

A parte, hay un test con dos metodos de prueba en "test/recomendador_test.go" Los dos fallan debido a que representan cambios
que queremos en nuestro codigo.

El test requiere que haga dos cambios funcionales:
- que la lista de recomendaciones no incluya las peliculas que ya ha visto el cliente
- que el formato csv cambie del actual <titulo, genero, director> a <titulo, director, genero>

Como observará son dos cambios diferentes: uno en el algoritmo de recomendacion, otro en la forma de
transmitir los datos. En la implementacion actual, los dos cambios deben realizarse en la misma clase,
violando el principio de "unica responsabilidad".

Refactorice el codigo para que estas dos responsabilidades esten en clases diferentes. 
Despues, adapta el codigo del test para la nueva estructura.